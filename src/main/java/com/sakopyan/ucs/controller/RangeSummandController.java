package com.sakopyan.ucs.controller;

import com.sakopyan.ucs.model.Range;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/range")
public class RangeSummandController {
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView get(@RequestParam(value = "start_range", required = true) String start_range,
                            @RequestParam(value = "end_range", required = true) String end_range) {

        Range r = new Range(start_range, end_range);

        ModelAndView mv = new ModelAndView("rangesummand");
        mv.addObject("start_range", r.getStart());
        mv.addObject("end_range", r.getEnd());
        mv.addObject("entity", r.getResult());
        return mv;
    }

}
