package com.sakopyan.ucs.model;

public class Entity implements Comparable<Entity>{
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + sum;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Entity other = (Entity) obj;
        if (sum != other.sum)
            return false;
        return true;
    }

    int x;
    int y;
    int sum;

    public Entity(int x, int y){
        this.x = x;
        this.y = y;
        this.sum = x + y;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int compareTo(Entity o) {
        if(sum > o.sum) {
            return 1;
        } else if(sum < o.sum) {
            return -1;
        } else {
            return 0;
        }
    }
}
