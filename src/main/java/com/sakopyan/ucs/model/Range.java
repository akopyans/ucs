package com.sakopyan.ucs.model;

import java.util.*;

public class Range {
    private int start;
    private int end;

    ArrayList<Integer> lsIntegers = new ArrayList<Integer>();

    public Range(String start, String end) {
        this.start = Integer.parseInt(start);
        this.end = Integer.parseInt(end);

        getIntegerFromInterval();
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    private void getIntegerFromInterval() {
        boolean[] primes = new boolean[end];
        Arrays.fill(primes, true);
        primes[0] = primes[1] = false;

        for (int i = 2; i < end; i++) {
            if (primes[i]) {
                for (int j = 2; i * j < end; j++) {
                    primes[i * j] = false;
                }
            }
        }

        for (int i = 0; i < end; i++) {
            if (primes[i]) {
                lsIntegers.add(i);
            }
        }
    }

    public TreeSet<Entity> getResult() {
        TreeSet<Entity> treeEntity = new TreeSet<Entity>();

        for (int i = 0; i < lsIntegers.size(); i++) {
            for (int j = 0; j < lsIntegers.size(); j++) {
                int sum = lsIntegers.get(i) + lsIntegers.get(j);
                if (start <= sum && sum <= end) {
                    treeEntity.add(new Entity(lsIntegers.get(i), lsIntegers.get(j)));
                }
            }
        }

        for (int i=start; i<end; i++) {
            Entity entity = new Entity(-1, -1);
            entity.setSum(i);
            treeEntity.add(entity);
        }

        return treeEntity;
    }
}
