<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>UCS - Test</title>
</head>
<body>
<center>
    range: ${start_range} - ${end_range}
    <c:forEach var="entity" items="${entity}">
        (${entity.getX()}, ${entity.getY()})
    </c:forEach>
</center>
</body>
</html>